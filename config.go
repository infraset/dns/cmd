package main

import (
	"os"
	"os/user"
	"fmt"
	"errors"

	"gopkg.in/yaml.v2"
)

type DNSServer struct {
	Name string `yaml:"name"`
	Address string `yaml:"address"`
	Default bool `yaml:"default"`
	DefaultZone string `yaml:"default_zone"`
}

type AppConfig struct {
	FilePath string
	Servers []DNSServer `yml:"servers"`
}

func (cfg *AppConfig) Server(name string) (server *DNSServer, err error) {

	for idx, s := range cfg.Servers {
		if s.Name == name {
			server = &cfg.Servers[idx]
			break
		}
	}

	if server == nil {
		err = errors.New("server does not exists")
	}

	return
}

func (cfg *AppConfig) DefaultServer() (server *DNSServer, err error) {
	
	for idx, s := range cfg.Servers {
		if s.Default {
			server = &cfg.Servers[idx]
			break
		}
	}

	if server == nil {
		err = errors.New("there is no default server")
	}

	return
}

func (cfg *AppConfig) AddServer(name string, address string) (err error) {

	s, e := cfg.Server(name)

	if e == nil {
		err = fmt.Errorf("already exists a server with name: %s (%s)\n", s.Name, s.Address)
		return
	}

	cfg.Servers = append(cfg.Servers, DNSServer{
		Name: name,
		Address: address,
	})

	return
}

func NewAppConfig() (config *AppConfig, err error) {

	config = &AppConfig{
		Servers: []DNSServer{},
	}

	usr, err := user.Current()

	if err != nil {
		return
	}

	// check if environment data is available and create it if not

	envDir := fmt.Sprintf("%s/.dnsctl", usr.HomeDir)
	
	config.FilePath = fmt.Sprintf("%s/config.yml", envDir)

	if _, err = os.Stat(envDir); err != nil {
		err = os.Mkdir(envDir, 0765)

		if err != nil {
			return
		}
	}

	// load config

	file, err := os.Open(config.FilePath)

	if err != nil {
		
		file, err = os.Create(config.FilePath)

		if err != nil {
			return
		}

		file.Write([]byte("\nservers:\n"))
	}
	defer file.Close()

	decoder := yaml.NewDecoder(file)

	err = decoder.Decode(&config)

	if err != nil {
		return
	}	

	return
}