module gitlab.com/infraset/dns/cmd

go 1.13

require (
	github.com/urfave/cli/v2 v2.1.1
	gitlab.com/infraset/dns/agent v1.0.1
	gopkg.in/yaml.v2 v2.2.2
)
