package main

import (
	"log"
	"os"
	"fmt"
	
	cli "github.com/urfave/cli/v2"
	"gitlab.com/infraset/dns/agent/api"
)

var globalConfig *AppConfig
var selectedServer *DNSServer

func main() {

	var err error

	globalConfig, err = NewAppConfig()

	if err != nil {
		log.Fatalf("failed to load config file: %v\n", err)
		os.Exit(1)
	}

	app := &cli.App {
		Name: "dnsctl",
		Usage: "A simple and centrilized dns controller tool",

		/* Application flags */

		Flags: []cli.Flag {
			&cli.StringFlag{
				Name: "server",
				Aliases: []string{"s"},
				Usage: "select a dns server by name",
			},
		},	

		/* Application Commands */

		Commands: []*cli.Command{

			/* servers */

			{
				Name: "servers",
				Usage: "list all available servers",
				Action: CmdServers,
			},

			/* list zones from selected server */

			{
				Name: "zones",
				Usage: "list zones",
				Action: CmdZones,
			},

			/* list records from selected server */

			{
				Name: "list",
				Aliases: []string{"ls", "l"},
				Usage: "list all records",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name: "zone",
						Aliases: []string{"z"},
						Usage: "zone name",
					},
					&cli.StringFlag{
						Name: "type",
						Aliases: []string{"t"},
						Value: "*",
						Usage: "record type (*|A|CNAME|TXT)",
					},
				},
				Action: CmdListRecords,
			},

			/* add record */

			{
				Name: "add",
				Aliases: []string{"a"},
				Usage: "add a new record",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name: "zone",
						Aliases: []string{"z"},
						Usage: "zone name",
					},
				},
				Action: CmdAddRecord,
			},

			/* delete record */

			{
				Name: "delete",
				Aliases: []string{"del", "d"}, 
				Usage: "delete an existing record",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name: "zone, z",
						Usage: "zone name",
					},
				},
				Action: CmdDeleteRecord,
			},
		},
	} // App

	err = app.Run(os.Args)

	if err != nil {
		fmt.Printf("\nERROR: %s\n\n", err.Error())
	}
}

func getClient(ctx *cli.Context) (client *api.Client, err error) {

	client = api.NewClient()

	serverName := ctx.String("server")

	if len(serverName) > 0 {
		selectedServer, err = globalConfig.Server(serverName)
	} else {
		selectedServer, err = globalConfig.DefaultServer()
	}

	if err != nil {
		return
	}

	err = client.Connect(fmt.Sprintf("%s", selectedServer.Address))

	return
}

func CmdServers(ctx *cli.Context) (err error) {

	fmt.Printf("\n* All DNS Servers Available:\n\n")
	fmt.Printf("[Name]               [Address]                [Default]\n\n")

	for _, srv := range globalConfig.Servers {

		isDefault := ""

		if srv.Default {
			isDefault = "*"
		}

		fmt.Printf("%-20s %-24s     %s\n", srv.Name, srv.Address, isDefault)
	}

	fmt.Printf("\n\n")

	return
}

func CmdZones(ctx *cli.Context) (err error) {

	return
}

func CmdListRecords(ctx *cli.Context) (err error) {

	var c *api.Client
	var records []api.Record

	c, err = getClient(ctx)

	if err != nil {
		return
	}
	defer c.Disconnect()

	zone := ctx.String("zone")

	if len(zone) == 0 {
		zone = selectedServer.DefaultZone
	}

	rtype := ctx.String("type")

	records, err = c.ListRecords(zone, rtype)

	if err != nil {
		return
	}

	rs := newRecordSet(records)
	rs.Print()

	return
}

func CmdAddRecord(ctx *cli.Context) (err error) {

	var c *api.Client

	c, err = getClient(ctx)

	if err != nil {
		return
	}
	defer c.Disconnect()

	zone := ctx.String("zone")

	if len(zone) == 0 {
		zone = selectedServer.DefaultZone
	}

	if len(zone) == 0 {
		err = fmt.Errorf("The zone need to be informed. Use -z <zone>\n\n")
		return
	}

	name := ctx.Args().Get(0)
	rtype := ctx.Args().Get(1)
	data := ctx.Args().Get(2)

	if len(name) == 0 || len(rtype) == 0 || len(data) == 0 {
		err = fmt.Errorf("Informe all parameters.\nSample:\n\tdnsctl [-z <zone>] <name> <type> <data>\n\n")
		return
	}

	err = c.AddRecord(zone, name, rtype, data)

	if err != nil {
		return
	}

	fmt.Printf("record created successfully.\n")

	return
}

func CmdDeleteRecord(ctx *cli.Context) (err error) {

	var c *api.Client

	c, err = getClient(ctx)

	if err != nil {
		return
	}
	defer c.Disconnect()

	zone := ctx.String("zone")

	if len(zone) == 0 {
		zone = selectedServer.DefaultZone
	}

	if len(zone) == 0 {
		err = fmt.Errorf("The zone need to be informed. Use -z <zone>\n\n")
		return
	}

	name := ctx.Args().Get(0)
	rtype := ctx.Args().Get(1)
	data := ctx.Args().Get(2)

	if len(name) == 0 || len(rtype) == 0 || len(data) == 0 {
		err = fmt.Errorf("Informe all parameters.\nSample:\n\tdnsctl [-z <zone>] <name> <type> <data>\n\n")
		return
	}

	err = c.DeleteRecord(zone, name, rtype, data)

	if err != nil {
		return
	}

	fmt.Printf("record deleted successfully.\n")

	return

	return
}



// Auxiliar structures

type RecordGroup struct {
	Name string
	Items []api.Record
	MaxNameLength int
}

func (rg *RecordGroup) AddItem(record api.Record) {

	if rg.Items == nil {
		rg.Items = make([]api.Record, 0)
	}

	rg.Items = append(rg.Items, record)

	recordNameLen := len(record.Name)

	if rg.MaxNameLength < recordNameLen {
		rg.MaxNameLength = recordNameLen
	}
}

func (rg *RecordGroup) Print() {

	if rg.Items == nil {
		return
	}

	itemCount := len(rg.Items)
	
	fmtstr := fmt.Sprintf("%%-%ds %%-10s %%s\n", rg.MaxNameLength + 5)

	fmt.Printf("\n\n* %s (%d records):\n\n", rg.Name, itemCount)

	for _, item := range rg.Items { 
		fmt.Printf(fmtstr, item.Name, item.Type, item.Data)
	}
}

type RecordSet struct {
	Groups map[string]*RecordGroup
}

func (rs *RecordSet) Add(record api.Record) {

	if _, ok := rs.Groups[record.Type]; !ok {
		rs.Groups[record.Type] = &RecordGroup{Name: record.Type}
	}

	rs.Groups[record.Type].AddItem(record)
}

func (rs *RecordSet) Print() {

	for _, g := range rs.Groups {
		g.Print()
	}

}

func newRecordSet(records []api.Record) (rs *RecordSet) {
	
	rs = &RecordSet{
		Groups: make(map[string]*RecordGroup),
	}

	for _, r := range records {
		rs.Add(r)
	}

	return
}